import numpy as np
from covid19_data import covid19_data
from bokeh.io import curdoc
from bokeh.layouts import column, row
#from bokeh.models import ColumnDataSource, Slider, TextInput, LegendItem, Legend, RadioGroup, Toggle, Button
from bokeh.models import *
from bokeh.plotting import figure, output_file
#from bokeh.themes import built_in_themes
import bokeh.plotting
import bokeh.layouts
import pandas as pd
from datetime import datetime

states = [
"Alabama",
"Alaska",
"Arizona",
"Arkansas",
"California",
"Colorado",
"Connecticut",
"Delaware",
"Florida",
"Georgia",
"Hawaii",
"Idaho",
"Illinois",
"Indiana",
"Iowa",
"Kansas",
"Kentucky",
"Louisiana",
"Maine",
"Maryland",
"Massachusetts",
"Michigan",
"Minnesota",
"Mississippi",
"Missouri",
"Montana",
"Nebraska",
"Nevada",
"New Hampshire",
"New Jersey",
"New Mexico",
"New York",
"North Carolina",
"North Dakota",
"Ohio",
"Oklahoma",
"Oregon",
"Pennsylvania",
"Rhode Island",
"South Carolina",
"South Dakota",
"Tennessee",
"Texas",
"Utah",
"Vermont",
"Virginia",
"Washington",
"West Virginia",
"Wisconsin",
"Wyoming",
]

#First state will be Alabama
state_data = covid19_data('Alabama')
#print(state_data.population)

source1=ColumnDataSource(dict(x=state_data.date_index, y=state_data.cumulative_positives))
source2=ColumnDataSource(dict(x=state_data.date_index, y=state_data.cumulative_deaths))
#source3=ColumnDataSource(dict(x=state_data.date_index, y=state_data.cumulative_tests))

#First comparison will be hubei
comparison_data = covid19_data('Hubei')
source4=ColumnDataSource(dict(x=[], y=[]))
source5=ColumnDataSource(dict(x=[], y=[]))

def get_plot(log_scale = True, normalized = False):
    # Set up plot
    if log_scale and not normalized:
        plot = figure(plot_width=600, plot_height=200, title="Alabama Totals",
                      tools="crosshair,pan,reset,save,wheel_zoom",
                      y_axis_type="log", y_range=[10**0, 10**6],
                      y_axis_label = "Individual Cases",
                      background_fill_color="#fafafa")

    if log_scale and normalized:
        plot = figure(plot_width=1000, plot_height=600, title="Alabama Totals",
                      tools="crosshair,pan,reset,save,wheel_zoom",
                      y_axis_type="log", y_range=[10**0, 10**2],)

    if not log_scale and not normalized:
        pass

    if not log_scale and normalized:
        pass

    plot.sizing_mode= 'scale_both'
    plot.line('x','y',source=source1, line_width=3, line_alpha=1, line_color="blue", line_dash="dashed")
    plot.line('x','y',source=source2, line_width=3, line_alpha=1, line_color="blue")
    #plot.line('x','y',source=source3, line_width=3, line_alpha=0.6, color='green')
    plot.line('x','y',source=source4, line_width=3, line_alpha=0.6, color="red", line_dash="dashed")
    plot.line('x','y',source=source5, line_width=3, line_alpha=0.6, color="red")

    li1 = LegendItem(label='Selected State Cumulative Cases', renderers=[plot.renderers[0]])
    li2 = LegendItem(label='Selected State Cumulative Deaths', renderers=[plot.renderers[1]])
    #li3 = LegendItem(label='Cumulative Administered Tests', renderers=[plot.renderers[2]])
    li4 = LegendItem(label='Selected Comparison Cumulative Cases', renderers=[plot.renderers[2]])
    li5 = LegendItem(label='Selected Comparison Cumulative Deaths', renderers=[plot.renderers[3]])
    legend1 = Legend(items=[li1, li2, li4, li5], location='top_right')
    plot.add_layout(legend1)

    return(plot)

plot = get_plot()

################################################################################
def state_select_change(attrname, old, new):
    key = state_select.value
    plot.title.text = key + " Totals"
    state_data = covid19_data(key)

    #print(state_data.population)
    if 0 in population_normalize_button.active:
        source1.data = dict(x=state_data.date_index, y=state_data.cumulative_positives_per_capita)
        source2.data = dict(x=state_data.date_index, y=state_data.cumulative_deaths_per_capita)
    else:
        source1.data=dict(x=state_data.date_index, y=state_data.cumulative_positives)
        source2.data=dict(x=state_data.date_index, y=state_data.cumulative_deaths)
    #source3.data=dict(x=state_data.date_index, y=state_data.cumulative_tests)

    #population_normalize_button.active=[]


state_select = Select(value='Alabama', options=states)
state_select.on_change('value', state_select_change)

def population_normalize_button_change(attrname, old, new):
    if 0 in population_normalize_button.active:
        plot.y_range.end=1
        plot.y_range.start=10**-5
        plot.yaxis.axis_label = "Percent of Population"
        source1.data = dict(x=state_data.date_index, y=state_data.cumulative_positives_per_capita)
        source2.data = dict(x=state_data.date_index, y=state_data.cumulative_deaths_per_capita)
        if 0 in comparison_enable_button.active:
            source4.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_positives_per_capita)
            source5.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_deaths_per_capita)
        else:
            source4.data = dict(x=[], y=[])
            source5.data = dict(x=[], y=[])
        #source3.data = dict(x=state_data.date_index, y=state_data.cumulative_tests_per_capita)

    else:
        plot.y_range.end=10**6
        plot.y_range.start=10**0
        plot.yaxis.axis_label = "Individual Cases"
        if 0 in comparison_enable_button.active:
            source4.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_positives)
            source5.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_deaths)
        else:
            source4.data = dict(x=[], y=[])
            source5.data = dict(x=[], y=[])
        source1.data = dict(x=state_data.date_index, y=state_data.cumulative_positives)
        source2.data = dict(x=state_data.date_index, y=state_data.cumulative_deaths)
        #source3.data = dict(x=state_data.date_index, y=state_data.cumulative_tests)


population_normalize_button = CheckboxButtonGroup(
        labels=["Normalize by Population"], active=[])

population_normalize_button.on_change('active', population_normalize_button_change)

row1 = row(state_select, population_normalize_button)

################################################################################
def comparison_enable_button_change(attrname, old, new):
    if 0 in comparison_enable_button.active:
        if 0 not in population_normalize_button.active:
            source4.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_positives)
            source5.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_deaths)
        else:
            source4.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_positives_per_capita)
            source5.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_deaths_per_capita)
    else:
        source4.data = dict(x=[], y=[])
        source5.data = dict(x=[], y=[])

comparison_enable_button = CheckboxButtonGroup(labels=["Compare"], active=[])

comparison_enable_button.on_change('active', comparison_enable_button_change)

def comparison_select_change(attrname, old, new):
    if comparison_select.value == 'Hubei':
        comparison_data = covid19_data(comparison_select.value)
    else:
        comparison_data = covid19_data(comparison_select.value, state=False)

    if 0 in comparison_enable_button.active:
        if 0 in population_normalize_button.active:
            source4.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_positives_per_capita)
            source5.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_deaths_per_capita)
        else:
            source4.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_positives)
            source5.data = dict(x=comparison_data.date_index, y=comparison_data.cumulative_deaths)

comparison_select = Select(value='Compare to:', options=["Hubei", "Italy", "South Korea", "Spain"])
comparison_select.on_change('value', comparison_select_change)

row2 = row(comparison_select, comparison_enable_button)
################################################################################
intro = Paragraph(text="""This is a simple tool to monitor and compare the infection rate of COVID-19.
Start by selecting your state of interest, your results will display automatically.
If you'd like to compare to other countries select one from the subsequent dropdown and hit compare.
It is also possible to normalize the comparison by population sizes.""")
intro2 = Paragraph(text="""This should help with assessing outbreak rate as well as how far along the outbreak is in the selected state.
Please note that the data has been shifted off of its calendar dates to help align the data during comparison.""")
intro3 = Paragraph(text="""Data is updated daily, so the right-most end of the selected state represents today.
Data is generated and updated by John Hopkins CSSE, and the COVID-19 Tracking Project.""")

window = column(intro, intro2, intro3, row1, row2, plot)
window.sizing_mode='scale_both'
curdoc().add_root(window)
curdoc().title = "corona-curve.com"
