import numpy as np
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
from collections import namedtuple

dataset = pd.read_csv('covid_19_data.csv')
hubei = dataset.loc[dataset['Province/State']=='Hubei']

hubei_population = 58500000
hubei_cumulative_positives = hubei['Confirmed'].to_list()
hubei_cumulative_deaths = hubei['Deaths'].to_list()
hubei_cumulative_recoveries = hubei['Recovered'].to_list()

hubei_cumulative_positives_per_capita = np.divide(hubei['Confirmed'] ,population).to_list()
hubei_cumulative_deaths_per_capita = np.divide(hubei['Deaths'], population).to_list()
hubei_cumulative_recoveries_per_capita = np.divide(hubei['Recovered'] ,population).to_list()

def days_between(d1, d2):
        return abs((d2 - d1).days)

_hubei_dates = hubei['ObservationDate'].to_list()
hubei_dates = []
for d  in _hubei_dates:
    hubei_dates.append(datetime.strptime(str(d),"%m/%d/%Y"))

hubei_date_index = []

for index, date_str in enumerate(hubei_dates):
    hubei_date_index.append(days_between(date_str, dates[0]))
