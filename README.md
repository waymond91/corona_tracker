# Corona Tracker
This is a small python-dash application served on heroku to that tracks covid 19 cases both domestically and internationally. 
I have many friends in and out of the US, so this created a small way for us to track and compare our relative situations 
in an informed, convenient way during the early stages of lockdown.

Data was compiled from John Hopkins CSSE, and the COVID-19 Tracking Project.

## Example Use
![Example GIF](thumbnail5.gif)