import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from datetime import datetime

pops ={
"California": 39937489,
"Texas": 29472295,
"Florida": 21992985,
"New York": 19440469,
"Pennsylvania": 12820878,
"Illinois": 12659682,
"Ohio": 11747694,
"Georgia": 10736059,
"North Carolina": 10611862,
"Michigan": 10045029,
"New Jersey": 8936574,
"Virginia": 8626207,
"Washington": 7797095,
"Arizona": 7378494,
"Massachusetts": 6976597,
"Tennessee": 6897576,
"Indiana": 6745354,
"Missouri": 6169270,
"Maryland": 6083116,
"Wisconsin": 5851754,
"Colorado": 5845526,
"Minnesota": 5700671,
"South Carolina": 5210095,
"Alabama": 4908621,
"Louisiana": 4645184,
"Kentucky": 4499692,
"Oregon": 4301089,
"Oklahoma": 3954821,
"Connecticut": 3563077,
"Utah": 3282115,
"Iowa": 3179849,
"Nevada": 3139658,
"Arkansas": 3038999,
"Mississippi": 2989260,
"Kansas": 2910357,
"New Mexico": 2096640,
"Nebraska": 1952570,
"Idaho": 1826156,
"West Virginia": 1778070,
"Hawaii": 1412687,
"New Hampshire": 1371246,
"Maine": 1345790,
"Montana": 1086759,
"Rhode Island": 1056161,
"Delaware": 982895,
"South Dakota": 903027,
"North Dakota": 761723,
"Alaska": 734002,
"Vermont": 628061,
"Wyoming": 567025,
"Hubei": 58500000,
"Italy" : 60484025,
"South Korea" : 51258138,
"Spain": 46750252
}

class covid19_data:
    dates = None
    name = None
    def __init__(self, name, state=True):
        self.name = name
        national_covid19 = pd.read_csv('covid_19_data.csv')

        if state:
            self.data = national_covid19.loc[national_covid19['Province/State']==name]
        else:
            self.data = national_covid19.loc[national_covid19['Country/Region']==name]
        self.population = pops[self.name]

        self.cumulative_positives = self.data['Confirmed'].to_list()
        self.cumulative_deaths = self.data['Deaths'].to_list()
        self.cumulative_recoveries = self.data['Recovered'].to_list()
        self.positives = np.subtract(self.data['Confirmed'], self.data['Recovered']).to_list()
        #print(self.cumulative_recoveries)

        self.cumulative_positives_per_capita = np.multiply(np.divide(self.data['Confirmed'] ,self.population),100).to_list()
        self.cumulative_deaths_per_capita = np.multiply(np.divide(self.data['Deaths'], self.population), 100).to_list()
        self.cumulative_recoveries_per_capita = np.multiply(np.divide(self.data['Recovered'] ,self.population), 100).to_list()
        self.positives_per_capita = np.divide(np.subtract(self.data['Confirmed'], self.data['Recovered']), self.population)



        _dates = self.data['ObservationDate'].to_list()
        self.dates = []
        for d  in _dates:
            self.dates.append(datetime.strptime(str(d),"%m/%d/%Y"))

        self.date_index = []
        for index, date_str in enumerate(self.dates):
            self.date_index.append(self.days_between(date_str, self.dates[0]))

        self.data_objects =[
            self.cumulative_positives,
            self.cumulative_deaths,
            self.cumulative_recoveries,
            self.positives,
            #print(,
            self.cumulative_positives_per_capita,
            self.cumulative_deaths_per_capita,
            self.cumulative_recoveries_per_capita,
            self.positives_per_capita,
            self.date_index,
            self.dates
        ]

        zero_count = 0
        for positive in self.cumulative_positives:
            if positive == 0:
                zero_count += 1
            else:
                break
        for data_object in self.data_objects:
            for i in range(zero_count):
                data_object.pop(0)

    def days_between(self, d1, d2):
        return abs((d2 - d1).days)


def update_data():
    pass
