import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import math
from datetime import datetime
from scipy.optimize import curve_fit
from pandas.plotting import register_matplotlib_converters
import os
import csv
import requests

register_matplotlib_converters()

state_pops ={
"California": 39937489,
"Texas": 29472295,
"Florida": 21992985,
"New York": 19440469,
"Pennsylvania": 12820878,
"Illinois": 12659682,
"Ohio": 11747694,
"Georgia": 10736059,
"North Carolina": 10611862,
"Michigan": 10045029,
"New Jersey": 8936574,
"Virginia": 8626207,
"Washington": 7797095,
"Arizona": 7378494,
"Massachusetts": 6976597,
"Tennessee": 6897576,
"Indiana": 6745354,
"Missouri": 6169270,
"Maryland": 6083116,
"Wisconsin": 5851754,
"Colorado": 5845526,
"Minnesota": 5700671,
"South Carolina": 5210095,
"Alabama": 4908621,
"Louisiana": 4645184,
"Kentucky": 4499692,
"Oregon": 4301089,
"Oklahoma": 3954821,
"Connecticut": 3563077,
"Utah": 3282115,
"Iowa": 3179849,
"Nevada": 3139658,
"Arkansas": 3038999,
"Mississippi": 2989260,
"Kansas": 2910357,
"New Mexico": 2096640,
"Nebraska": 1952570,
"Idaho": 1826156,
"West Virginia": 1778070,
"Hawaii": 1412687,
"New Hampshire": 1371246,
"Maine": 1345790,
"Montana": 1086759,
"Rhode Island": 1056161,
"Delaware": 982895,
"South Dakota": 903027,
"North Dakota": 761723,
"Alaska": 734002,
"Vermont": 628061,
"Wyoming": 567025
}

states = {
"Alabama" :	"AL",
"Alaska" :	"AK",
"Arizona" :	"AZ",
"Arkansas" :	"AR",
"California" :	"CA",
"Colorado" :	"CO",
"Connecticut" :	"CT",
"Delaware" :	"DE",
"Florida" :	"FL",
"Georgia" :	"GA",
"Hawaii" :	"HI",
"Idaho" :	"ID",
"Illinois" :	"IL",
"Indiana" :	"IN",
"Iowa" :	"IA",
"Kansas" :	"KS",
"Kentucky" :	"KY",
"Louisiana" :	"LA",
"Maine" :	"ME",
"Maryland" :	"MD",
"Massachusetts" :	"MA",
"Michigan" :	"MI",
"Minnesota" :	"MN",
"Mississippi" :	"MS",
"Missouri" :	"MO",
"Montana" :	"MT",
"Nebraska" :	"NE",
"Nevada" :	"NV",
"New Hampshire" :	"NH",
"New Jersey" : "NJ",
"New Mexico" : "NM",
"New York" : "NY",
"North Carolina" :	"NC",
"North Dakota" :	"ND",
"Ohio" :	"OH",
"Oklahoma" :	"OK",
"Oregon" :	"OR",
"Pennsylvania" :	"PA",
"Rhode Island" :	"RI",
"South Carolina" :	"SC",
"South Dakota" :	"SD",
"Tennessee" :	"TN",
"Texas" :	"TX",
"Utah" :	"UT",
"Vermont" :	"VT",
"Virginia" :	"VA",
"Washington" :	"WA",
"West Virginia" :	"WV",
"Wisconsin" :	"WI",
"Wyoming" :	"WY"
}
states = {y:x for x,y in states.items()}

#us_covid19 = pd.read_csv('us_states_covid19_daily.csv')

class state_covid19_data:
    dates = None
    state_abbreviation = None

    def __init__(self, state_abbreviation):

        self.state_abbreviation = state_abbreviation

        url = 'http://covidtracking.com/api/states/daily.csv'
        us_covid19 = pd.read_csv(url)

        self.data = us_covid19.loc[us_covid19['state']==state_abbreviation.upper()]
        self.population = state_pops[states[state_abbreviation]]

        _dates = self.data['date']
        self.dates = []
        for d  in _dates:
            self.dates.append(datetime.strptime(str(d),"%Y%m%d"))

        self.cumulative_positives = self.data['positive'].to_list()
        self.cumulative_deaths = self.data['death'].to_list()
        self.cumulative_tests = self.data['totalTestResults'].to_list()

        self.cumulative_positives_per_capita = np.divide(self.data['positive'], self.population).to_list()
        self.cumulative_deaths_per_capita = np.divide(self.data['death'], self.population).to_list()
        self.cumulative_tests_per_capita = np.divide(self.data['totalTestResults'], self.population).to_list()

        # Convert calendar dates to a number for curve fitting
        self.date_index = []
        for index, date_str in enumerate(self.dates):
            self.date_index.append(self.days_between(date_str, self.dates[0]))
        self.date_index.reverse()

    def fetch_data(self):
        url = 'http://covidtracking.com/api/states/daily.csv'
        us_covid19 = pd.read_csv(url)

        self.data = us_covid19.loc[us_covid19['state']==self.state_abbreviation.upper()]
        _dates = self.data['date']
        self.dates = []
        for d  in _dates:
            self.dates.append(datetime.strptime(str(d),"%Y%m%d"))

        self.cumulative_positives = self.data['positive'].to_list()
        self.cumulative_deaths = self.data['death'].to_list()
        self.cumulative_tests = self.data['totalTestResults'].to_list()

        # Convert calendar dates to a number for curve fitting
        self.date_index = []
        for index, date_str in enumerate(self.dates):
            self.date_index.append(self.days_between(date_str, self.dates[0]))
        self.date_index.reverse()

    def days_between(self, d1, d2):
        return abs((d2 - d1).days)

    def fit(self):
        # Get the log of the positive cases
        self.log_cumulative_positives = []
        for c in self.cumulative_positives:
            # Cannot take log of 0
            if c == 0:
                print('Ugh')
                self.log_cumulative_positives.append(None)
                continue
            self.log_cumulative_positives.append(math.log(c))

        # Define the curve fitting function
        def line(x, m, b):
            return(m*x+b)

        # Curve fit
        self.popt, self.pcov = curve_fit(line,  self.date_index, self.log_cumulative_positives, p0=[1.0, 1.0],  maxfev=100000)
        #print(popt)

        self.log_linear_model = []
        for i in self.date_index:
            self.log_linear_model.append(line(i, *self.popt))

        self.residuals = np.subtract(self.log_cumulative_positives,self.log_linear_model)

    def log_plot(self):
        plt.figure(figsize=(20,10))
        plt.semilogy(self.dates, self.cumulative_positives)
        plt.semilogy(self.dates, self.cumulative_deaths)
        plt.semilogy(self.dates, self.cumulative_tests)
        plt.xticks(rotation=45)
        plt.grid(True, which="both")
        plt.legend(["Total Patients","Total Deaths", "Total Test Results"])
        plt.title(self.state_abbreviation.upper()+" Cumulative Counts")
        plt.show()

    def plot(self):
        plt.figure(figsize=(20,10))
        plt.plot(self.dates, self.cumulative_positives)
        plt.plot(self.dates, self.cumulative_deaths)
        plt.plot(self.dates, self.cumulative_tests)
        plt.xticks(rotation=45)
        plt.grid(True, which="both")
        plt.legend(["Total Patients","Total Deaths", "Total Test Results"])
        plt.title(self.state_abbreviation.upper()+" Cumalitve Counts")
        plt.show()

    def fit_plot(self):
        plt.figure(figsize=(20,10))
        plt.subplot(2,1,1)
        plt.plot(self.date_index, self.log_cumulative_positives)
        plt.plot(self.date_index, self.log_linear_model)
        plt.legend(['Log Fit','Total Patients'])
        plt.title(self.state_abbreviation.upper()+" Log linear Curve Fit")
        plt.subplot(2,1,2)
        plt.plot(self.date_index, self.residuals)
        plt.legend(['Residuals'])
        plt.title(self.state_abbreviation.upper()+" Inflection Points")
        plt.show()

if __name__ == '__main__':
    abrev = input("Please enter a 2 character US State abbreviation (i.e. 'CA' for California): ")
    state = state_covid19_data(abrev)
    state.fetch_data()
    state.fit()
    state.log_plot()
    state.plot()
    state.fit_plot()
    input("Press Enter to quit...")
